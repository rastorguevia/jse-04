package ru.rastorguev.tm.context;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.view.View.*;

import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.enumerated.Command;
import ru.rastorguev.tm.repository.ProjectRepository;
import ru.rastorguev.tm.repository.TaskRepository;
import ru.rastorguev.tm.service.ProjectService;
import ru.rastorguev.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Bootstrap {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private TaskRepository taskRepository = new TaskRepository();
    private TaskService taskService = new TaskService(taskRepository);
    private ProjectRepository projectRepository = new ProjectRepository(taskRepository);
    private ProjectService projectService = new ProjectService(projectRepository);

    public void init() throws IOException {
        launchConsole();
    }

    public void launchConsole() throws IOException {
        showWelcomeMsg();

        while (true) {
            Command command = Command.EMPTY;
            try {
                String input = reader.readLine();
                command = Command.valueOf(input.toUpperCase());
            }catch (IllegalArgumentException e) {
                commandExecution(Command.UNKNOWN);
            }
            if (Command.EXIT.equals(command)) {
                reader.close();
                System.exit(0);
            }
            commandExecution(command);
        }
    }

    public void commandExecution (Command command) throws IOException {
        switch (command) {
            case HELP:
                showHelpMsg();
                break;
            case PROJECT_CREATE:
                createProject();
                break;
            case PROJECT_LIST:
                listProjects();
                break;
            case PROJECT_SELECT:
                selectProject();
                break;
            case PROJECT_EDIT:
                editProject();
                break;
            case PROJECT_REMOVE:
                removeProject();
                break;
            case PROJECT_CLEAR:
                clearProjects();
                break;
            case TASK_CREATE:
                createTask();
                break;
            case TASK_LIST:
                listTasks();
                break;
            case TASK_SELECT:
                selectTask();
                break;
            case TASK_EDIT:
                editTask();
                break;
            case TASK_REMOVE:
                removeTask();
                break;
            case TASK_CLEAR:
                clearProjectTasks();
                break;
            case UNKNOWN:
                showUnknownCommandMsg();
                break;
        }
    }

    public void createProject() throws IOException {
        System.out.println("Project create");
        System.out.println("Enter name");
        Project project = new Project();
        project.setName(reader.readLine());
        System.out.println("Enter description");
        project.setDescription(reader.readLine());
        System.out.println("Add date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Add start date DD.MM.YYYY? Y/N");
            if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter start date");
                project.setStartDate(stringToDate(reader.readLine()));
            }
            System.out.println("Add end date DD.MM.YYYY? Y/N");
            if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter end date");
                project.setEndDate(stringToDate(reader.readLine()));
            }
        }
        projectService.persist(project);
        System.out.println("OK");
    }

    public void listProjects() {
        System.out.println("Project list");
        printAllProjects(projectService.findAll());
        System.out.println("OK");
    }

    public void selectProject() throws IOException {
        System.out.println("Project select");
        System.out.println("Enter project ID");
        printAllProjects(projectService.findAll());
        Project project = projectService.findOne(projectService.getProjectIdByNumber(Integer.parseInt(reader.readLine())));
        printProject(project);
        System.out.println("OK");
    }

    public void editProject() throws IOException{
        System.out.println("Project edit");
        System.out.println("Enter project ID");
        printAllProjects(projectService.findAll());
        Project project = projectService.findOne(projectService.getProjectIdByNumber(Integer.parseInt(reader.readLine())));
        Project editedProject = new Project();
        editedProject.setId(project.getId());
        System.out.println("Edit name? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter project name");
            editedProject.setName(reader.readLine());
        } else editedProject.setName(project.getName());
        System.out.println("Edit description? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter new description");
            editedProject.setDescription(reader.readLine());
        } else editedProject.setDescription(project.getDescription());
        System.out.println("Edit start date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter start date");
            editedProject.setStartDate(stringToDate(reader.readLine()));
        } else editedProject.setStartDate(project.getStartDate());
        System.out.println("Edit end date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter end date");
            editedProject.setEndDate(stringToDate(reader.readLine()));
        } else editedProject.setEndDate(project.getEndDate());
        projectService.merge(editedProject);
        System.out.println("OK");
    }

    public void removeProject() throws IOException {
        System.out.println("Project remove");
        System.out.println("Enter ID");
        printAllProjects(projectService.findAll());
        String projectId = projectService.getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        projectService.remove(projectId);
        System.out.println("OK");
    }

    public void clearProjects() {
        projectService.removeAll();
        System.out.println("OK");
    }

    public void createTask() throws IOException {
        System.out.println("Task create");
        System.out.println("Enter Project ID");
        printAllProjects(projectService.findAll());
        Task task = new Task(projectService.getProjectIdByNumber(Integer.parseInt(reader.readLine())));
        System.out.println("Enter task name");
        task.setName(reader.readLine());
        System.out.println("Enter task description");
        task.setDescription(reader.readLine());
        System.out.println("Add date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Add start date DD.MM.YYYY? Y/N");
            if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter start date");
                task.setStartDate(stringToDate(reader.readLine()));
            }
            System.out.println("Add end date DD.MM.YYYY? Y/N");
            if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter end date");
                task.setEndDate(stringToDate(reader.readLine()));
            }
        }
        taskService.persist(task);
        System.out.println("OK");
    }

    public void listTasks() throws IOException {
        System.out.println("Task list");
        System.out.println("Enter Project ID");
        printAllProjects(projectService.findAll());
        String projectId = projectService.getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        printTaskListByProjectId(projectId, taskService.findAll());
        System.out.println("OK");
    }

    public void selectTask() throws IOException {
        System.out.println("Task select");
        System.out.println("Enter Project ID");
        printAllProjects(projectService.findAll());
        String projectId = projectService.getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        printTaskListByProjectId(projectId, taskService.findAll());
        List<Task> filteredTaskList = taskService.filterTaskListByProjectId(projectId, taskService.findAll());
        System.out.println("Enter Task ID");
        String taskId = taskService.getTaskIdByNumber(Integer.parseInt(reader.readLine()),filteredTaskList);
        printTask(taskService.findOne(taskId));
        System.out.println("OK");
    }

    public void editTask() throws IOException {
        System.out.println("Task edit");
        System.out.println("Enter Project ID");
        printAllProjects(projectService.findAll());
        String projectId = projectService.getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        printTaskListByProjectId(projectId, taskService.findAll());
        List<Task> filteredTaskList = taskService.filterTaskListByProjectId(projectId, taskService.findAll());
        System.out.println("Enter Task ID");
        String taskId = taskService.getTaskIdByNumber(Integer.parseInt(reader.readLine()),filteredTaskList);
        Task task = taskService.findOne(taskId);
        Task editedTask = new Task(task.getProjectId());
        editedTask.setId(task.getId());
        System.out.println("Edit name? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter task name");
            editedTask.setName(reader.readLine());
        } else editedTask.setName(task.getName());
        System.out.println("Edit description? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter new description");
            editedTask.setDescription(reader.readLine());
        } else editedTask.setDescription(task.getDescription());
        System.out.println("Edit start date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter start date");
            editedTask.setStartDate(stringToDate(reader.readLine()));
        } else editedTask.setStartDate(task.getStartDate());
        System.out.println("Edit end date? Y/N");
        if (Command.Y.equals(Command.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter end date");
            editedTask.setEndDate(stringToDate(reader.readLine()));
        } else editedTask.setEndDate(task.getEndDate());
        taskService.merge(editedTask);
        System.out.println("OK");
    }

    public void removeTask() throws IOException {
        System.out.println("Task remove");
        System.out.println("Enter Project ID");
        printAllProjects(projectService.findAll());
        String projectId = projectService.getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        printTaskListByProjectId(projectId, taskService.findAll());
        List<Task> filteredTaskList = taskService.filterTaskListByProjectId(projectId, taskService.findAll());
        System.out.println("Enter Task ID");
        String taskId = taskService.getTaskIdByNumber(Integer.parseInt(reader.readLine()),filteredTaskList);
        taskService.remove(taskId);
        System.out.println("OK");
    }

    public void clearProjectTasks() throws IOException {
        System.out.println("Enter Project ID");
        printAllProjects(projectService.findAll());
        String projectId = projectService.getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        taskService.removeTaskListByProjectId(projectId);
        System.out.println("OK");
    }
}